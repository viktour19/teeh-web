// Base routes for default index/root path, about page, 404 error pages, and others..
var trainController = require('./../controllers/trainController');
var predictController = require('./../controllers/predictController');
var mapController = require('./../controllers/mapController');

var Joi = require('joi');

exports.register = function(server, options, next){

    server.route([
        {
            method: 'GET',
            path: '/about',
            config: {
                handler: function(request, reply){
                    reply.view('about', {
                        title: 'About'
                    });
                },
                id: 'about'
            }
        },
        {
            method: 'GET',
            path: '/',
            config: {
                handler: function(request, reply){
                  // Render the view with the custom greeting
                    reply.view('index', {
                        title: 'Teeh'
                    });
                },
                id: 'index'
            }
        },
        
        {
            method: 'GET',
            path: '/potholemap',
            config: {
                handler: function(request, reply){
                    reply.view('potholemap', {
                        title: 'Pothole Map'
                    });
                },
                id: 'potholemap'
            }
        },
        
        {
            method: 'GET',
            path: '/trafficmap',
            config: {
                handler: function(request, reply){
                    reply.view('trafficmap', {
                        title: 'Traffic Map'
                    });
                },
                id: 'trafficmap'
            }
        },
        {
            method: 'POST',
            path: '/potholemap',
            config: {
                handler: mapController.prototype.loadpotholemap,
                id: 'potholemapfilter'
            }
        },
        
        {
            method: 'POST',
            path: '/trafficmap',
            config: {
                handler: mapController.prototype.loadtrafficmap,
                id: 'trafficmapfilter'
            }
        }
        ,
        {
            method: 'GET',
            path: '/{path*}',
            config: {
                handler: function(request, reply){
                    reply.view('404', {
                        title: 'Total Bummer 404 Page'
                    }).code(404);
                },
                id: '404'
            }
        },
             {
            method: 'GET',
            path: '/trainspeedbreaker',
            config: {
                handler: trainController.prototype.trainSpeedbreaker,
                id: 'trainspeedbreaker'
            }
        },
             {
            method: 'GET',
            path: '/trainpothole',
            config: {
                handler: trainController.prototype.trainPothole,
                id: 'trainpothole'
            }
        }
        ,
             {
            method: 'POST',
            path: '/predictpothole',
            config: {
                handler: predictController.prototype.predictPothole,
                validate: {
                    payload : {
                    uniquedataid: Joi.string()
                    }},
                id: 'predictpothole'
            }
        }
        ,
             {
            method: 'POST',
            path: '/predictSpeedbreaker',
            config: {
                handler: predictController.prototype.predictSpeedbreaker,
                 validate:{
                    payload : {
                    uniquedataid: Joi.string()
                    }},
                id: 'predictSpeedbreaker'
            }
        }
    ]);

    next();
}

exports.register.attributes = {
    name: 'base'
};