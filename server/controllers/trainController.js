"use strict";

var svm = require('node-svm');
var so = require('stringify-object')
var Q = require('q');
var fs = require('fs');
var path = require('path')

module.exports = trainController;

function trainController() {

}

trainController.prototype =
{
    trainSpeedbreaker: function (request, reply) {
        
        var output = ""
        var trainfile = path.join(__dirname,'datasets/speedbreakertraindataset.json');
        var testfile = path.join(__dirname,'datasets/speedbreakertestdataset.json');
        var modelfile = path.join(__dirname,'model/speedbreakermodel.json');


        var clf = new svm.CSVC({
            gamma: 0.25,
            c: 1, // allow you to evaluate several values during training
            normalize: true,
            reduce: false,
            kFold: 1 // disable k-fold cross-validation
        });

        Q.all([
            svm.read(trainfile),
            svm.read(testfile)
        ]).spread(function (trainingSet, testingSet) {
            return clf.train(trainingSet)
                .progress(function (progress) {
                console.log('training progress: %d%', Math.round(progress * 100));
            }).spread(function (trainedModel, trainingReport) {
                var wstream = fs.createWriteStream(modelfile);
                wstream.write(so(trainedModel));
                wstream.end();
            })
                .then(function () {
                return clf.evaluate(testingSet);
            });
        }).done(function (evaluationReport) {
            output = so(evaluationReport);
            console.log('Accuracy against the Speedbreaker testset:\n', output);
            reply.view('trainoutput', {
                         viewoutput: output,
                          title: "Speedbreaker Dataset Trained & Evaluated Successfully"                                              
                    });
        });       
       
    },

    trainPothole: function (request, reply) {
        
        var output = "";
        var trainfile = path.join(__dirname,'datasets/potholetraindataset.json');
        var testfile = path.join(__dirname,'datasets/potholetestdataset.json');
        var modelfile = path.join(__dirname,'model/potholemodel.json');

        var clf = new svm.CSVC({
            gamma: 0.25,
            c: 1, // allow you to evaluate several values during training
            normalize: true,
            reduce: false,
            kFold: 1 // disable k-fold cross-validation
        });

        Q.all([
            svm.read(trainfile),
            svm.read(testfile)
        ]).spread(function (trainingSet, testingSet) {
            return clf.train(trainingSet)
                .progress(function (progress) {
                console.log('training progress: %d%', Math.round(progress * 100));
            }).spread(function (trainedModel, trainingReport) {
                var wstream = fs.createWriteStream(modelfile);
                wstream.write(so(trainedModel));
                wstream.end();
            })
                .then(function () {
                return clf.evaluate(testingSet);
            });
        }).done(function (evaluationReport) {
            output = so(evaluationReport);
            console.log('Accuracy against the Pothole testset:\n', output);
             reply.view('trainoutput', {                  
                        viewoutput: output,
                        title: "Pothole Dataset Trained & Evaluated Successfully"
                    });
        });
        
      
    }
};
