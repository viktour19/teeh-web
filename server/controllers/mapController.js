"use strict";
var Hapi = require('hapi');
var Parse = require('node-parse-api').Parse;
var so = require('stringify-object')
var fs = require('fs');
var jshare = require('jshare');

var path = require('path');
var appDir = path.dirname(require.main.filename);


module.exports = mapController;

function mapController() {
	var app = new Parse(options);
	return app;
}


mapController.prototype =
{
	loadtrafficmap: function (request, reply) {
		var options = {
			app_id: 'Gxm9lWsZd1x5GPH4PRfSKz7Jb9AU7YKd0MxlIuSP',
			master_key: 'n9H8xerL9eYO5TqFpLIUD0eOlQ9X6A0kvWBEPIoO' // master_key:'...' could be used too
		};

		//var options = require(path.join(appDir, 'server')).parseoptions;
		//console.log(options);
		
		var app = new Parse(options);

		var data = { lat: 24.6408, lng: 46.7728, count: 3 };
		var datelimit = request.payload.datelimit;
		var geojson = Array();
		var query = {
			where:
			{
				speedbreaker: '1'
			},
			lessThan:
			{
				timestamp: datelimit
			}
		};

		app.find('Teeh', query,
			function (err, response) {
				console.log('success');
				var count = response.results.length;
				var results = response.results;
				for (var index = 0; index < count.length; index++) {
					var element = results[index];
					data.lng = element.longitude;
					data.lat = element.latitude;
					data.count = 1;

					geojson[index] = data;
					reply(geojson)
						.type('application/json');

				}
			
			});
	},
	loadpotholemap: function (request, reply) {
		var options = require(path.join(appDir, 'server')).parseoptions;
		var app = new Parse(options);
		var datafile = path.join(appDir, 'public/js/data.geojson');
		var geoobj = {
			"type": "Feature",
			"geometry": {
				"type": "Point",
				"coordinates": [
				]
			},
			"properties": {
				"title": "",
				"description": "",
				"timestamp": "",
				"location": "",
				"marker-color": "#fc4353",
				"marker-size": "small",
				"marker-symbol": "star"
			}
		};
		var geojson = Array();
		var datelimit = request.payload.datelimit;
		var query =
			{
				where:
				{
					pothole: '1'
				},
				lessThan:
				{
					timestamp: datelimit
				}
			};
		new app.Query("TeehData");

		query.find('Teeh', query, function (err, response) {
				var results = response.results;
				var count = results.length;

				for (var index = 0; index < count.length; index++) {
					var element = results[index];
					geoobj.geometry.coordinates[0] = element.longitude;
					geoobj.geometry.coordinates[1] = element.latitude;
					geoobj.properties.timestamp = element.timestamp;

					geojson[index] = geoobj;

				}
			
		});
		var wstream = fs.createWriteStream(datafile);
		wstream.write(so(geojson));
		wstream.end();

		reply.view('potholemap', {
			title: 'Pothole Map',
			geojson: geojson
		});
	}
}