"use strict";

var svm = require('node-svm');
var Parse = require('node-parse-api').Parse;

var path = require('path');
var appDir = path.dirname(require.main.filename);
var options = require (path.join(appDir,'server')).parseoptions;


module.exports = predictController;
var app;

function predictController() {
	app = new Parse(options);
}

predictController.prototype =
{
	predictSpeedbreaker: function (request, reply) {
		var uniquedataid = request.payload.uniquedataid;
		var modelfile = path.join(__dirname,'model/speedbreakermodel.json');
		var query =
			{
				where:
				{
					uniquedataid: uniquedataid
				},
				count: 1
			};

		app.find('TeehData', query, function (err, response) {
			console.log(response);

			var count = response.count;
			var result = response.results;
			var data = Array();
			var dataArray = Array();

			for (var i = 0; i < count; i++) {
				data[i][0] = result[i].mean_x;
				data[i][1] = result[i].mean_y;
				data[i][2] = result[i].mean_z;
				dataArray[i] = data;
			}

			svm.read(modelfile)
				.then(function (model) {
					var newClf = svm.restore(model);

				dataArray.forEach(function (ex) {
					var prediction = newClf.predictSync(ex[0]);
					console.log('   %d, %d, %d => %d', ex[0][0], ex[0][1], ex[0][2], prediction);
					var objectId = ex[1];
					app.updateUser(objectId, { speedbreaker: prediction }, 'sessionToken', function (err, response) {
						console.log(response);
					});
				});
			});
		});
	},
	predictPothole: function (request, reply) {
		var uniquedataid = request.payload.uniquedataid;
		var modelfile = path.join(__dirname,'model/potholemodel.json');
		var query =
			{
				where:
				{
					uniquedataid: uniquedataid
				},
				count: 1
			};

		app.find('TeehData', query, function (err, response) {
			console.log(response);

			var count = response.count;
			var result = response.results;
			var data = Array();
			var dataArray = Array();

			for (var i = 0; i < count; i++) {

				data[i][0] = result[i].x;
				data[i][1] = result[i].y;
				data[i][2] = result[i].z;
				dataArray[i][0] = data;
				dataArray[i][1] = result.objectId;

			}

			svm.read(modelfile)
				.then(function (model) {
				var newClf = svm.restore(model);

				dataArray.forEach(function (ex) {
					var prediction = newClf.predictSync(ex[0]);
					console.log('   %d, %d, %d => %d', ex[0][0], ex[0][1], ex[0][2], prediction);
					var objectId = ex[1];
					app.updateUser(objectId, { pothole: prediction }, 'sessionToken', function (err, response) {
						console.log(response);
					});
				});

			});
		});
	}
};