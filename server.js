/**
* Dependencies.
*/
var Hapi = require('hapi');
var jshare = require('jshare');

// Create a new server
var server = new Hapi.Server();

// Setup the server with a host and port
server.connection({
    port: parseInt(process.env.PORT, 10) || 3000,
    host: '0.0.0.0'
});

// Setup the views engine and folder
server.views({
    engines: {
        html: require('swig')
    },
    path: './server/views'
});
var parseoptions = {
    app_id: 'Gxm9lWsZd1x5GPH4PRfSKz7Jb9AU7YKd0MxlIuSP',
    master_key: 'n9H8xerL9eYO5TqFpLIUD0eOlQ9X6A0kvWBEPIoO' // master_key:'...' could be used too
};

// Export the server to be required elsewhere.
module.exports = server;
module.exports = parseoptions;

server.register([
    {
        register: require("good"),
        options: {
            opsInterval: 5000,
            reporters: [{
                reporter: require('good-console'),
                args:[{ ops: '*', request: '*', log: '*', response: '*', 'error': '*' }]
            }]
        }
    },
    {
        register: require("hapi-assets"),
        options: require('./assets.js')
    },
    {
        register: require("hapi-named-routes")
    },
    {
        register: require("hapi-cache-buster")
    },
    {
        register: require('./server/assets/index.js')
    },
    {
      register: require('./server/base/index.js')
    }
], function () {
    //Start the server
    server.start(function() {
        //Log to the console the host and port info
        console.log('Server started at: ' + server.info.uri);
        
    });
});
